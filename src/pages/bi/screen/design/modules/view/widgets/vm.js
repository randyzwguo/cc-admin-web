/**
 * 执行一段字符串格式的函数
 */
export function runFnInVm(code, echarts) {
  const result = {};
  const fn = new Function('echarts', `return ${code}`); // eslint-disable-line no-new-func
  try {
    result.value = fn(echarts);
  } catch (e) {
    result.error = e;
  }
  return result;
}
